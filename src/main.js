import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/tailwind.css'
import CheckboxInput from "@/components/inputs/CheckboxInput";
import TextInput from "@/components/inputs/TextInput";
import RadioInput from "@/components/inputs/RadioInput";
import TwoSideInput from "@/components/inputs/TwoSideInput";
import CommonBtn from "@/components/common/CommonBtn";
import Vuelidate from 'vuelidate'
import CommonBtnOutline from "@/components/common/CommonBtnOutline";
import SelectInput from "@/components/inputs/SelectInput";

Vue.config.productionTip = false
Vue.use(Vuelidate)
Vue.component('checkbox-input', CheckboxInput)
Vue.component('text-input', TextInput)
Vue.component('radio-input', RadioInput)
Vue.component('select-input', SelectInput)
Vue.component('two-side-input', TwoSideInput)
Vue.component('common-btn', CommonBtn)
Vue.component('common-btn-outline', CommonBtnOutline)


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
