import Vue from 'vue'
import VueRouter from 'vue-router'
import PageQuiz from "@/views/PageQuiz";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    redirect: {name: 'quiz'}
  },
  {
    path: '/quiz',
    name: 'quiz',
    component: PageQuiz
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
