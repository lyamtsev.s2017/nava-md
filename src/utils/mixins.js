import {mapGetters, mapMutations} from "vuex";
import {constants} from "@/constants";

export const checkboxMixin = {
  data() {
    return {
      checkboxSelected: [],
      checkHandler: () => {
        throw 'No handler specified for checkbox'
      }
    }
  },
  mounted() {
    this.$watch('checkboxSelected', this.checkHandler, {
      deep: true
    })
  },
  methods: {
    handleCheckboxInput(val) {
      if (this.checkboxSelected.includes(val)) {
        const idx = this.checkboxSelected.findIndex(item => item === val)
        this.checkboxSelected.splice(idx, 1)
      } else {
        this.checkboxSelected.push(val)
      }
    },
    // Used for choices with NONE_CHOICE
    filterChoices(newVal) {
      if (newVal.length > 1 && newVal.includes(constants.NONE_CHOICE)) {
        const idx = newVal.indexOf(constants.NONE_CHOICE)
        if (idx === 0) {
          newVal.splice(idx, 1)
        } else {
          newVal = [constants.NONE_CHOICE]
        }
      }

      return newVal
    },
    nextStage() {
      this.$emit('nextStage')
    }
  }
}

export const textareaMixin = {
  data() {
    return {
      inputText: null,
      inputTextHandler: () => {
        throw 'No handler specified for textarea'
      }
    }
  },
  mounted() {
    this.$watch('inputText', this.inputTextHandler)
  },
  methods: {
    nextStage() {
      this.$emit('nextStage')
    }
  }
}

export const radioMixin = {
  data() {
    return {
      radioSelected: null,
      radioHandler: () => {
        throw 'No handler specified for radio btn'
      }
    }
  },
  mounted() {
    this.$watch('radioSelected', this.radioHandler, {
      deep: true
    })
  },
  methods: {
    nextStage() {
      this.$emit('nextStage')
    }
  }
}

export const answersMixin = {
  methods: {
    ...mapMutations([
      'SET_ANSWERS',
      'SET_ANSWER',
    ]),
    setAnswer(answerKey, value, valueKey) {
      this.SET_ANSWER({answerKey, value, valueKey})
    }
  },
  computed: {
    ...mapGetters([
      'answers',
      'answer',
    ]),
    answersKeys() {
      return constants.ANSWERS_KEYS
    },
  },
}

export const parentComponentMixin = {
  data() {
    return {
      stages: {},
      stageNumber: 1,
    }
  },
  methods: {
    nextStage() {
      if (this.stageNumber >= this.maxStages) {
        this.$emit('nextStage')
      } else {
        this.stageNumber++
      }
    },
    goToStage(number) {
      if (number >= this.maxStages) {
        this.$emit('nextStage')
      } else {
        this.stageNumber = number
      }
    },
  },
  computed: {
    getCurrentComponent() {
      return this.stages[this.stageNumber]
    },
    maxStages() {
      return Object.keys(this.stages).length
    }
  }
}

export const referMixin = {
  computed: {
    ...mapGetters([
      'refer'
    ])
  },
  methods: {
    ...mapMutations([
      'SET_REFER'
    ]),
    setRefer(title, info) {
      this.SET_REFER({title, info})
    },
    clearRefer() {
      this.SET_REFER(null)
    }
  }
}

export const mainDataMixin = {
  computed: {
    ...mapGetters([
      'answer',
    ]),
    answersKeys() {
      return constants.ANSWERS_KEYS
    },
    selectedTreatment() {
      return this.answer(this.answersKeys.ACCOUNT_CREATION)?.treatmentName
    },
    userFirstName() {
      return this.answer(this.answersKeys.ACCOUNT_CREATION)?.stage_2?.firstName
    },
    userLastName() {
      return this.answer(this.answersKeys.ACCOUNT_CREATION)?.stage_2?.lastName
    },
    userBirthdate() {
      return this.answer(this.answersKeys.ACCOUNT_CREATION)?.stage_3?.birthdate
    },
    userGender() {
      return this.answer(this.answersKeys.ACCOUNT_CREATION)?.stage_4
    }
  },
}

export const offerMixin = {
  computed: {
    ...mapGetters([
      'offerText'
    ])
  },
  methods: {
    ...mapMutations([
      'SET_OFFER_TEXT'
    ]),
  }
}
