const colors = require('tailwindcss/colors')

module.exports = {
  purge: { content: ['./public/**/*.html', './src/**/*.vue'] },
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      transparent: 'transparent',
      black: {
        DEFAULT: '#111C31',
        dark: '#222222'
      },
      red: {
        light: '#CC0000',
      },
      green: {
        lightest: '#D1E0D7',
        light: '#B4CDBF',
        'light-2': '#B1CABB',
        'light-3': '#D1E0D7',
        lighter: '#94B1A1',
        DEFAULT: '#7FE4CF',
        darker: '#78D8C4',
        dark: '#72CCB9',
        'sdark': '#025157',
        black: '#014045',
        'n-black': '#244C5A',
      },
      white: colors.white,
      grey: {
        light: '#E8E8E8',
        DEFAULT: '#C4C4C4',
        mlight: '#7D7D7D',
        medium: '#767676',
        'medium-2': '#646464',
        darker: '#E2E2E2',
        dark: '#717171'
      },
      primary: {
        DEFAULT: '#F9F8F9'
      },
      misc: {
        1: '#B5C7C9'
      }
    },
    boxShadow: {
      input: '0 2px 8px 0 rgba(0, 0, 0, 0.04)',
      'input-hover-pressed': '0 2px 8px 0 rgba(0, 0, 0, 0.08)',
    },
    fontFamily: {
      'sans': ['Greycliff CF', 'sans-serif'],
      'quincy': ['Quincy CF', 'serif']
    },
    extend: {
      maxWidth: {
        quiz: '500px',
      },
      fontSize: {
        '4.5xl': ['42px', '44px'],
        'xxs': '10px'
      },
      letterSpacing: {
        '12px': 1.2
      },
      backgroundImage: theme => ({
        checkmark: "url('~@/assets/img/icons/tick.svg')",
      }),
      gridTemplateColumns: {
        input: '24px 1fr',
        navbar: '1fr minmax(1fr, 500px) 1fr',
        'mp-content': '1fr 300px',
        payment: '16px 2fr 1fr 1fr',
        results: '40px 1fr'
      },
    },
  },
  variants: {
    extend: {
      borderWidth: ['active', 'hover'],
      cursor: ['disabled'],
      opacity: ['disabled', 'active'],
      backgroundColor: ['active', 'disabled', 'group-focus'],
      color: ['group-focus'],
    },
  },
  plugins: [],
}
